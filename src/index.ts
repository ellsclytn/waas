import axios, { AxiosResponse } from 'axios'
import { CronJob } from 'cron'
import { promises as fs } from 'fs'
import { PlaylistItemsResponse } from './interfaces'

const PLAYLIST_ID = 'PLy3-VH7qrUZ5IVq_lISnoccVIYZCMvi-8'
const PAGE_TOKEN_FILE = './pageToken'

const {
  YOUTUBE_API_KEY,
  SLACK_TOKEN,
  DISCORD_WEBHOOK_TOKEN,
  DISCORD_WEBHOOK_ID,
  SERVICE_NAME,
  TIMEZONE = 'Australia/Melbourne',
  TIME_OF_DAY = '9'
} = process.env

/**
 * Attempt to load the page token from file. Warn if it doesn't exist.
 */
const loadPageToken = async () => {
  try {
    return await fs.readFile(PAGE_TOKEN_FILE, 'utf8')
  } catch (err) {
    console.warn('Failed to load pageToken. Additional queries may be required.')
  }
}

/**
 * Attempt to fetch the latest Wednesday from youtube
 * @param pageToken optional page token from previous requests
 */
const fetchVideo = async (pageToken?: string) => {
  const params = {
    key: YOUTUBE_API_KEY,
    maxResults: 25,
    pageToken,
    part: 'snippet',
    playlistId: PLAYLIST_ID
  }

  try {
    const { data }: AxiosResponse<PlaylistItemsResponse> =
      await axios.get('https://www.googleapis.com/youtube/v3/playlistItems', { params })

    console.log('Fetched page with token:', pageToken)

    if (data.nextPageToken) {
      return fetchVideo(data.nextPageToken)
    }

    // non-blocking action
    fs.writeFile(PAGE_TOKEN_FILE, pageToken)
      .catch((err) => {
        console.warn('Failed to save pageToken. Additional queries may be required on restart.', err)
      })

    return Promise.resolve(`https://www.youtube.com/watch?v=${data.items[data.items.length - 1].snippet.resourceId.videoId}`)
  } catch (err) {
    return Promise.reject(err)
  }
}

/**
 * Attempt to post to the SERVICE_NAME webhook urls
 */
const postToWebhook = async () => {
  let token
  let videoUrl

  // get content for webhook payload
  try {
    token = await loadPageToken()
    videoUrl = await fetchVideo(token)
  } catch (err) {
    console.error('Unable to fetch content for webhook payload')
    return Promise.reject(err)
  }

  // work out which service to post to and return the post action
  switch (SERVICE_NAME) {
    case 'slack':
      return axios.post(`https://hooks.slack.com/services/${SLACK_TOKEN}`, {
        text: videoUrl
      })
    case 'discord':
      return axios.post(`https://discordapp.com/api/webhooks/${DISCORD_WEBHOOK_ID}/${DISCORD_WEBHOOK_TOKEN}`, {
        content: videoUrl
      })
    default:
      return Promise.reject('SERVICE_NAME must be set to "slack" or "discord"')
  }
}

const job = new CronJob({
  cronTime: `0 0 ${TIME_OF_DAY} * * 3`,
  onTick: () => {
    postToWebhook().catch((err) => {
      console.error(err)
      process.exit(1)
    })
  },
  timeZone: TIMEZONE
})

job.start()
