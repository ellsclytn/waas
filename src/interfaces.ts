// TODO: there's more to this than meets the eye. TRANSFORMERS
export interface PlaylistItemsResponse {
  nextPageToken?: string
  items: Array<{
    snippet: {
      resourceId: {
        videoId: string
      }
    }
  }>
}
